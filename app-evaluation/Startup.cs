﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(app_evaluation.Startup))]
namespace app_evaluation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
