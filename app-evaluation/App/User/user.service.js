﻿appEvaluation.factory('user.service',
    ["$http", function ($http) {
        var service = {
            insert: function (userData) {
                return $http({
                    url: "/User/Create",
                    method: "POST",
                    data: userData
                }).then(function (response) {
                    return response.data;
                }, function errorCallback(response) {
                 
                });
            },
            getAll: function () {
                return $http({
                    url: "/User/GetAll/",
                    method: "GET"
                }).then(function (response) {
                    return response.data;
                }, function errorCallback(response) {

                });
            },
            edit: function (userData) {
                return $http({
                    url: "/User/Edit",
                    method: "POST",
                    data: userData
                }).then(function (response) {
                    return response.data;
                }, function errorCallback(response) {

                });
            },
            delete: function (userData) {
                return $http({
                    url: "/User/Delete",
                    method: "POST",
                    data: userData
                }).then(function (response) {
                    return response.status;
                }, function errorCallback(response) {

                });
            }
        }

        return service;
    }])