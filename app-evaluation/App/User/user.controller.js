﻿appEvaluation.controller('user.controller', UserController);
UserController.$inject = ['$scope', 'user.service'];

function UserController($scope, $userService) {
    //variables
    $scope.users = [];
    $scope.user = { status: 'active', gender: 'male' };
    $scope.userSelected = {};
    getAll();
    $scope.isPasswordInvalid = false;
    $scope.showAlertExists = false;

    //functions
    function getAll() {
        $userService.getAll().then(function (response) {
            $scope.users = response.data;
        });
    }

    $scope.save = function (user, frmCreate) {
        if (frmCreate.$valid) {
            var userData = {
                user_name: user.userName,
                password: user.password,
                name: user.name,
                last_name: user.lastName,
                email: user.email,
                status: user.status ? true : false,
                gender: user.gender
            };

            $userService.insert(userData).then(function (response) {
                if (response.status) {
                    location.href = "/User/Index"
                } else {
                    $scope.showAlertExists = true;
                }
            });
        }
    }

    $scope.getDataUser = function (user) {
        $scope.userSelected = user;
    };

    $scope.edit = function (userSelected, frmEditUser) {
        if (frmEditUser.$valid) {
            var userData = {};
            if (userSelected.password) {
                if (angular.equals(userSelected.password, userSelected.confirmPassword)) {
                    userData.password = userSelected.password;
                }
                else {
                    $scope.isPasswordInvalid = true;
                    return false;
                }
            }

            userData = {
                id: userSelected.id,
                user_name: userSelected.user_name,
                email: userSelected.email,
                status: userSelected.status === 'active' ? true : false,
            };

            $userService.edit(userData).then(function (response) {
                    location.href = "/User/Index"
            });
        }
    }

    $scope.deleteUser = function (idUser) {
        $userService.delete({ id: idUser }).then(function (response) {
            location.reload();
        });
    }

    $scope.closeModal = function () {
        $('#editModal').modal('hide')
        $scope.userSelected = {};
        $scope.isPasswordInvalid = false;
    }
}