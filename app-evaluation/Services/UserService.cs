﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using app_evaluation.data;
using app_evaluation.Helpers;
using app_evaluation.Models;
using app_evaluation.Models.App;

namespace app_evaluation.Services
{
    public class UserService
    {
        public static List<UserViewModel> GetAll()
        {
            using (var _context = new evaluation_axsisEntities())
            {
                var users = _context.user.AsQueryable();

                return users.Select(user => new UserViewModel
                {
                    id = user.Id,
                    user_name = user.UserName,
                    name = user.FullName,
                    last_name = user.LastName,
                    email = user.Email,
                    gender =user.Gender,
                    status = user.UserStatus
                }).ToList();
            }
        }

        public static UserViewModel GetById(int id)
        {
            UserViewModel userVm = null;
            using (var _context = new evaluation_axsisEntities())
            {
                var user = _context.user.AsQueryable().Where(_user => _user.Id == id)
                                                      .FirstOrDefault();
                if(user!= null)
                {
                    userVm = new UserViewModel
                    {
                        id = user.Id,
                        user_name = user.UserName,
                        name = user.FullName,
                        last_name = user.LastName,
                        email = user.Email,
                        gender = user.Gender,
                        status = user.UserStatus
                    };
                }

                return userVm;
            }
        }

        public static UserViewModel GetValidatedUser(LoginViewModel loginVm)
        {
            UserViewModel userVm = null;
            using (var _context = new evaluation_axsisEntities())
            {
                var encryptedPassword = Encrypt.GetSHA256(loginVm.Password);
                var validatedUser = _context.user.FirstOrDefault(_user => _user.UserName.ToUpper().Equals(loginVm.UserName.ToUpper())
                                                                && _user.Password.Equals(encryptedPassword)
                                                                && _user.UserStatus);
                if (validatedUser != null)
                {
                    userVm = new UserViewModel
                    {
                        id = validatedUser.Id,
                        user_name = validatedUser.FullName,
                        email = validatedUser.Email,
                    };
                }
            }

            return userVm;
        }

        public static bool AlreadyExits(UserViewModel userVm)
        {
            bool exits = false;
            using (var _context = new evaluation_axsisEntities())
            {
                var user = _context.user.FirstOrDefault(_user => _user.UserName.ToUpper().Equals(userVm.user_name.ToUpper())
                                               || _user.Email.ToUpper().Equals(userVm.email.ToUpper()));

                exits = user != null;
            }

            return exits;
        }

        public static bool Delete(int id)
        {
            bool isDeleted = false;
            using (var _context = new evaluation_axsisEntities())
            {
                var user = _context.user.FirstOrDefault(_user => _user.Id == id);
                if(user != null)
                {
                    user.UserStatus = false;
                    isDeleted = _context.SaveChanges() > 0;
                }
            }

            return isDeleted;
        }

        public static bool Edit(UserViewModel userViewModel)
        {
            bool isUpdated = false;
            using (var _context = new evaluation_axsisEntities())
            {
                var user = _context.user.Where(_user => _user.Id == userViewModel.id).FirstOrDefault();
                if(user!= null)
                {
                    user.UserName = userViewModel.user_name;
                    if(userViewModel.password != null)
                        user.Password = userViewModel.password;
                    user.Email = userViewModel.email;
                    user.UserStatus = userViewModel.status;
                    user.UpdatedAt = DateTime.Now;

                    isUpdated = _context.SaveChanges() > 0;
                }
            }

            return isUpdated;
        }

        public static bool Insert(UserViewModel userViewModel, out int? idUser)
        {
            using (var _context = new evaluation_axsisEntities())
            {
                idUser = 0;
                bool isInserted = false;
                var user = new user()
                {
                    UserName = userViewModel.user_name,
                    Password = Encrypt.GetSHA256(userViewModel.password),
                    FullName = userViewModel.name,
                    LastName = userViewModel.last_name,
                    ThirdName = userViewModel.third_name,
                    Gender = userViewModel.gender,
                    Email = userViewModel.email,
                    UserStatus = userViewModel.status,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };

                _context.user.Add(user);
                isInserted = _context.SaveChanges() > 0;
                if (isInserted)
                    idUser = user.Id;

                return isInserted;
            }
        }
    }
}