﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using app_evaluation.Models.App;

namespace app_evaluation.Helpers
{
    public class SessionData
    {
        public static UserSession User
        {
            get
            {
                return GetValue<UserSession>("User");
            }
            set
            {
                SetValue("User", value);
            }
        }

        private static void SetValue<TValue>(string sessionKey, TValue value)
        {
            HttpContext.Current.Session[sessionKey] = value;
        }

        private static TResult GetValue<TResult>(string sessionKey)
        {
            try
            {
                var value = HttpContext.Current.Session[sessionKey];
                if (value != null)
                    return (TResult)Convert.ChangeType(value, typeof(TResult));
            }
            catch { }

            return default(TResult);
        }
    }
}