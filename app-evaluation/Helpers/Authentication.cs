﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using app_evaluation.Models.App;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace app_evaluation.Helpers
{
    public class Authentication
    {
        public static void SignIn(UserSession user, bool rememberMe)
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.NameIdentifier, Convert.ToString(user.Id)),
                new Claim(ClaimTypes.Name, user.UserName)
            }, DefaultAuthenticationTypes.ApplicationCookie);

            GetAuthenticationManager().SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = rememberMe,
                ExpiresUtc = DateTime.UtcNow.AddDays(1)
            }, identity);
        }

        public static void SignOut()
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Session.Abandon();
            if (HttpContext.Current.Request.Cookies["app_evaluation"] != null)
            {
                HttpCookie cookie = new HttpCookie("app_evaluation")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                HttpContext.Current.Response.Cookies.Add(cookie);
            }

            GetAuthenticationManager()
                .SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public static UserSession RecoveryUser()
        {
            var claims = GetAuthenticationManager()
                .User.Claims.ToList();

            if (!claims.Any())
            {
                return null;
            }

            var name = claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value ?? string.Empty;
            var email = claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value ?? string.Empty;
            var nameIdentifier = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value ?? string.Empty;
            //var rolCode = claims.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value ?? string.Empty;

            if (string.IsNullOrEmpty(nameIdentifier))
            {
                return new UserSession(0, null, null);
            }

            int idUser = 0;
            idUser = Convert.ToInt16(nameIdentifier);

            return new UserSession(idUser, nameIdentifier, email);
        }

        public static void ValidateUser()
        {
            if (IsAuthenticated() && SessionData.User == null)
            {
                SessionData.User = RecoveryUser();
            }
        }

        public static bool IsAuthenticated()
        {
            return GetAuthenticationManager().User.Identity.IsAuthenticated;
        }

        private static IAuthenticationManager GetAuthenticationManager()
        {
            return HttpContext.Current.GetOwinContext().Authentication;
        }
    }
}