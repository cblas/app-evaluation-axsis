﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app_evaluation.Models.App
{
    public class UserViewModel
    {
        public int id { get; set; }
        public string user_name { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string last_name { get; set; }
        public string third_name { get; set; }
        public string email { get; set; }
        public bool status { get; set; }
        public string gender { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}