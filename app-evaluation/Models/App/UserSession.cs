﻿namespace app_evaluation.Models.App
{
    public class UserSession
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public UserSession(int id, string username, string email)
        {
            this.Id = id;
            this.UserName = username;
            this.Email = email;
        }
    }
}