﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using app_evaluation.Models.App;
using app_evaluation.Services;

namespace app_evaluation.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAll()
        {
            try
            {
                var users = UserService.GetAll();
                return Json(new { status = true, data = users }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = "error server" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Create(UserViewModel userViewModel)
        {
            try
            {
                bool isCreated = UserService.Insert(userViewModel, out int? idUser);

                return Json(new { status = isCreated, id = idUser, message = isCreated ? "created" : "fail insert" });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = "Server Error" });
            }
        }

        public ActionResult Edit(int idUser)
        {
            var user = UserService.GetById(idUser);
            return View(user);
        }

        [HttpPost]
        public JsonResult Edit(UserViewModel userViewModel)
        {
            try
            {
                if (UserService.AlreadyExits(userViewModel))
                    return Json(new { status = false, message = "Ya existe un usuario registrado con el mismo username y password" });

                bool isUpdated = UserService.Edit(userViewModel);

                return Json(new { status = isUpdated, message = isUpdated ? "updated" : "fail update" });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = "Server Error" });
            }
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                bool isDeleted = UserService.Delete(id);

                return Json(new { status = isDeleted });
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = "Server Error" });
            }
        }
    }
}