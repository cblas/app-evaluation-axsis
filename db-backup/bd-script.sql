USE [master]
GO
/****** Object:  Database [evaluation_axsis]    Script Date: 22/10/2019 10:09:33 ******/
CREATE DATABASE [evaluation_axsis]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'evaluation_axsis', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\evaluation_axsis.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'evaluation_axsis_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\evaluation_axsis_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [evaluation_axsis].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [evaluation_axsis] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [evaluation_axsis] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [evaluation_axsis] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [evaluation_axsis] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [evaluation_axsis] SET ARITHABORT OFF 
GO
ALTER DATABASE [evaluation_axsis] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [evaluation_axsis] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [evaluation_axsis] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [evaluation_axsis] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [evaluation_axsis] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [evaluation_axsis] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [evaluation_axsis] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [evaluation_axsis] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [evaluation_axsis] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [evaluation_axsis] SET  ENABLE_BROKER 
GO
ALTER DATABASE [evaluation_axsis] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [evaluation_axsis] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [evaluation_axsis] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [evaluation_axsis] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [evaluation_axsis] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [evaluation_axsis] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [evaluation_axsis] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [evaluation_axsis] SET RECOVERY FULL 
GO
ALTER DATABASE [evaluation_axsis] SET  MULTI_USER 
GO
ALTER DATABASE [evaluation_axsis] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [evaluation_axsis] SET DB_CHAINING OFF 
GO
ALTER DATABASE [evaluation_axsis] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [evaluation_axsis] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [evaluation_axsis] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'evaluation_axsis', N'ON'
GO
USE [evaluation_axsis]
GO
/****** Object:  Table [dbo].[user]    Script Date: 22/10/2019 10:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [nvarchar](200) NULL,
	[FullName] [varchar](80) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[ThirdName] [varchar](50) NULL,
	[Email] [varchar](20) NOT NULL,
	[UserStatus] [bit] NOT NULL,
	[Gender] [varchar](20) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedAt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [evaluation_axsis] SET  READ_WRITE 
GO
